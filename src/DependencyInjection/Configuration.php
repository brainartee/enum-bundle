<?php

namespace Brainart\EnumBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        // TreeBuilder::root() is deprecated since symfony 4.3, not supported by 5.0 (method removed)
        // root is now passed into constructor, which didn't exist previously, therefore:
        if (method_exists(TreeBuilder::class, '__construct')) {
            $treeBuilder = new TreeBuilder('brainart_enum');
        } else {
            $treeBuilder = new TreeBuilder();
            $treeBuilder->root('brainart_enum');
        }

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.

        return $treeBuilder;
    }
}
