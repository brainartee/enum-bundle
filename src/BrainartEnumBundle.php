<?php

namespace Brainart\EnumBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use \Brainart\Enum\Model\Translatable\Symfony\TranslatableEnum;
use \Brainart\Enum\Model\Persistable\Doctrine\DoctrineEnum;

class BrainartEnumBundle extends Bundle
{
    public function boot()
    {
        parent::boot();
        // TODO: inject properly
        TranslatableEnum::setTranslator($this->container->get('translator'));
        DoctrineEnum::setDb($this->container->get('doctrine'));
    }
}
