<?php

namespace Brainart\EnumBundle\Component\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class EnumItem extends Constraint
{
    public ?string $message = 'The value "{{ item }}" is not a valid enum "{{ enum }}" item.';

    /**
     * @var string Enum class to validate
     */
    public string $enum;

    public function __construct(string $enum, mixed $options = null, ?array $groups = null, mixed $payload = null, ?string $message = null)
    {
        $options['enum'] = $enum;
        parent::__construct($options, $groups, $payload);
        $this->enum = $enum;
        $this->message = $message;

    }


    /**
     * {@inheritdoc}
     */
    public function getDefaultOption()
    {
        return 'enum';
    }

    /**
     * {@inheritdoc}
     */
    public function getRequiredOptions()
    {
        return ['enum'];
    }
}
