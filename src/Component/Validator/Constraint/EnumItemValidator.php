<?php

namespace Brainart\EnumBundle\Component\Validator\Constraint;

use Brainart\Enum\Model\Enum;
use Brainart\Enum\Model\Item;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class EnumItemValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof EnumItem) {
            throw new UnexpectedTypeException($constraint, EnumItem::class);
        }

        // custom constraints should ignore null and empty values to allow
        // other constraints (NotBlank, NotNull, etc.) take care of that
        if (null === $value || '' === $value) {
            return;
        }

        $enumClass = $constraint->enum;
        if (!is_a($enumClass, Enum::class, true)) {
            $message = (array)sprintf('Invalid value for option "%s" in constraint %s. Expecting child of class %s', 'enum', \get_class($constraint), Enum::class);
            // a common mistake
            if (is_a($enumClass, Item::class, true)) {
                $message[] = sprintf('Did you feed instance of %s instead?', Item::class);
            }
            $message = join('. ', $message);

            throw new InvalidOptionsException($message, ['enum']);
        }

        $itemClass = $enumClass::getItemClass();
        /** @var Enum $enum */
        $enum = new $enumClass();

        if (is_string($value)) {
            if (!$enum->has($value)) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ enum }}', $enumClass)
                    ->setParameter('{{ item }}', $value)
                    ->addViolation();
            }
        } elseif (!is_object($value) || !$value instanceof $itemClass) {
            throw new UnexpectedValueException($value, $itemClass);
        }
    }
}
