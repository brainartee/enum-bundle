<?php

namespace Brainart\EnumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('BrainartEnumBundle:Default:index.html.twig');
    }
}
