# Validation

To validate if model contains valid Enum Item:

Say you have Enum XLanguageEnum:

```php
<?php

namespace App\Entity;

use Brainart\Enum\Model\Translatable\Symfony\TranslatableEnum;

class XLanguageEnum extends TranslatableEnum
{
    const ESTONIAN = 'ESTONIAN';
    const ENGLISH = 'ENGLISH';

    protected static $items = [
        self::ESTONIAN => 'xlanguage.estonian',
        self::ENGLISH => 'xlanguage.english'
    ];
}
```

Then [validation](https://symfony.com/doc/current/validation.html) with annotation goes like 
this:

```php
use Brainart\EnumBundle\Component\Validator\Constraint as EnumItemAssert;

# Doctrine Entity
class Person
{
    // ...
    
    /**
     * @EnumItemAssert\EnumItem("App\Entity\XLanguageEnum")
     */
    protected $xLanguageId;
}
```

To customize error message:

```php
use Brainart\EnumBundle\Component\Validator\Constraint as EnumItemAssert;

# Doctrine Entity
class Person
{
    // ...
    
    /**
     * @EnumItemAssert\EnumItem(
     *   enum="App\Entity\XLanguageEnum", 
     *   message="Invalid language"
     * )
     */
    protected $xLanguageId;
}
```

