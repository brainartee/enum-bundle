# Enum Bundle for Symfony

## Setup

1) Define repository url for Enum Bundle:

```
composer config repositories."brainart/enum-bundle" vcs https://bitbucket.org/brainartee/enum-bundle.git
```

1a) Define same thing for [Enum](https://bitbucket.org/brainartee/enum). This is because dependencies cannot declare their own private repositories. 
Refer to [Enum's installation instructions](https://bitbucket.org/brainartee/enum).

2) Require enum bundle:
```
composer require "brainart/enum-bundle"
```

3) Include Bundle in your AppKernel:
```
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            ...
            new Brainart\EnumBundle\BrainartEnumBundle(),
            ...
```

4) Work

See [Enum Library](https://bitbucket.org/brainartee/enum) for details.

* [Using validator](docs/validation.md)
